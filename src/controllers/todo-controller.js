const service = require("../services/todo-service");

exports.get = (request, response) => {
    console.log("Get");
    response.send("OK Get");
}

exports.get_all = async (request, response) => {
    try {
        const todos = await service.get_all_todos();

        if(!todos) return response.status(404).json("Todos não encontrados");
        response.json(todos);
    }catch(e)
    {
        return response.status(500).json(e);
    }
}

exports.add = async (request, response) => {
    try
    {
        const todo_criado = await service.add_todo(request.body);
        response.status(201).json(todo_criado);
    } catch (e) {
        response.status(500).json(e);
    }
}
exports.update = (request, response) => {
    console.log("Update");
    response.send("OK Update");
}

exports.delete = (request, response) => {
    console.log("Delete");
    response.send("OK Delete");
}
