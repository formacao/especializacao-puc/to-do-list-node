const app = require("./app");
const banco_de_dados = require("./mongoosedb");
const porta = process.env.PORT || 3000;

banco_de_dados.init();

app.listen(porta, function() {
    console.log(`Escutando a porta ${porta}...`)
});