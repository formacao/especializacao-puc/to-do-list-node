const mongoose = require("mongoose");

const user = "admin";
const senha = "3lSEHg7x1YvfWYah";
const banco = "todo_sample";
const cluster = "cluster0.ovpdh.mongodb.net";

let connection_string = `mongodb+srv://${user}:${senha}@${cluster}/${banco}?retryWrites=true&w=majority`;

module.exports = {
    init: () => {
        mongoose.connect(connection_string,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
                useCreateIndex: true
            })
            .then((request) => console.log("Tudo certo!"))
            .catch((error) => `Deu erro aqui, chefe: ${error}`);
    }
}