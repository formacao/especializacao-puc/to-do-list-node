const todo = require("../models/todo");


module.exports = class Todo_Service {
    static async get_all_todos() {
        try {
            const all_todos = await todo.find();
            return all_todos;
        }catch (e) {
            console.log(`Deu erro: ${e}`);
        }
    }

    static async add_todo(data) {
        try {
            const new_todo = {
                title: data.title,
                description: data.description,
                date: data.date,
                finished: data.finished
            };
            const response = await new todo(new_todo).save();
            return response;
        } catch (e) {
            console.log(`Erro: ${e}`);
        }
    }
}